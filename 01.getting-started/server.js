// The Hello world! program of fastify

// Import the fastify
import Fastify from 'fastify';


// Create a new instance of Fastify
const fastify = Fastify({ 
    logger: true
});

// Hello World! route
fastify.get('/', async (request, reply) => {
    return { hello: 'world' };
});

// Run the server!
const start = async () => {
    try {
        await fastify.listen(3000);
        fastify.log.info(`server listening on ${fastify.server.address().port}`);
    } catch (err) {
        fastify.log.error(err);
        process.exit(1);
    }
};
start();