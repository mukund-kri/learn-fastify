const Author = require('../models').Author;


const getAllAuthors = async (request, reply) => {
    const authors = await Author.findAll();
    reply.send(authors);
};

const getAuthorById = async (request, reply) => {
    const author = await Author.findByPk(request.params.id);
    if (author) {
        reply.send(author);
    } else {
        reply.code(404).send({message: 'Author not found'});
    }
};

const addAuthor = async (request, reply) => {
    const author = await Author.create(request.body);
    reply.code(201).send(author);
};

const updateAuthor = async (request, reply) => {
    const author = await Author.findByPk(request.params.id);
    if (author) {
        await author.update(request.body);
        reply.send(author);
    } else {
        reply.code(404).send({message: 'Author not found'});
    }
};

const deleteAuthor = async (request, reply) => {
    const author = await Author.findByPk(request.params.id);
    if (author) {
        await author.destroy();
        reply.code(204).send();
    } else {
        reply.code(404).send({message: 'Author not found'});
    }
};

module.exports = {
    getAllAuthors,
    getAuthorById,
    addAuthor,
    updateAuthor,
    deleteAuthor,
}