const Book = require('../models').Book;
const { Op } = require('sequelize');


// Get all books from the database
const getAllBooks = async (request, reply) => {
    return await Book.findAll();
};

// /books handler
const getBooks = async (request, reply) => {
    const searchString = request.query.title;

    if (searchString) {
        const books = await Book.findAll({
            where: {
                title: {
                    [Op.iLike]: `%${searchString}%`
                }
            }
        });
        reply.send(books);
    } else {
        reply.send(await getAllBooks());
    };
};

// get book by id
const getBookById = async (request, reply) => {
    const bookId = request.params.id;

    const book = await Book.findByPk(bookId);
    if (!book) {
        reply.code(404).send({
            message: 'Book not found'
        });
    } else {
        reply.send(book);
    };
};

// Add a new book to the database
const addBook = async (request, reply) => {

    reply.code(201).send(await Book.create(request.body));
};


// Update an existing book in the database
const updateBook = async (request, reply) => {
    const bookId = request.params.id;
    const book = await Book.findByPk(bookId);
    // update book
    if (!book) {
        reply.code(404).send({message: 'Book not found'});
    } else {
        await book.update(request.body);
        reply.send(book);
    }
};

// Delete a book from the database
const deleteBook = async (request, reply) => {
    const bookId = request.params.id;
    const book = await Book.findByPk(bookId);

    if (!book) {
        reply.code(404).send({message: 'Book not found'});
    } else {
        await book.destroy();
        reply.send({message: 'Book deleted'});
    }
};


module.exports = {
    getBooks,
    getBookById,
    addBook,
    deleteBook,
    updateBook,
}
