'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('books', [{
      isbn: '9780321877581',
      title: 'The Lord of the Rings',
      description: 'The Lord of the Rings is an epic high fantasy novel written by English author J. R. R. Tolkien.',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      isbn: '9780553212333',
      title: 'The Hobbit',
      description: 'The Hobbit, or There and Back Again, is a children\'s fantasy novel by English author J. R. R. Tolkien.',
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('books', null, {});
  }
};
