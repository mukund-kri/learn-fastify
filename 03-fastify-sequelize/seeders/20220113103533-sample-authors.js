'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('authors', [{
      firstname: 'J.K.',
      middlename: 'R.',
      lastname: 'Rowling',
      honorific: 'Mz.',
      bio: 'J.K. Rowling is a British author, screenwriter and film producer. She is best known for her Harry Potter series',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      firstname: 'J.R.R.',
      middlename: 'Tolkien',
      lastname: 'Tolkien',
      honorific: 'Mr.',
      bio: 'A very old fantasy writer',
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('authors', null, {});
  }
};
