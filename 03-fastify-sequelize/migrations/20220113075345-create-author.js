'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('authors', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstname: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      middlename: {
        type: Sequelize.STRING(100),
        allowNull: true,
      },
      lastname: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      honorific: {
        type: Sequelize.STRING(10),
        allowNull: true,
      },
      bio: {
        type: Sequelize.STRING(4096),
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('authors');
  }
};