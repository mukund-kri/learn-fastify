'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class book extends Model {
    
    static associate(models) {
      book.belongsToMany(models.Author, {
        through: 'books_authors',
        foreignKey: 'book_id',
        as: 'authors'
      });
    }
  };
  book.init({
    isbn: DataTypes.STRING,
    title: DataTypes.STRING,
    
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Book',
    tableName: 'books',
  });
  return book;
};