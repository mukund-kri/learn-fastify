'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Author extends Model {

    static associate(models) {
      Author.belongsToMany(models.Book, { 
        through: 'books_authors',
        foreignKey: 'author_id',
        as: 'books'
      });

    
    }
  };
  
  Author.init({
    firstname: DataTypes.STRING,
    middlename: DataTypes.STRING,
    lastname: DataTypes.STRING,
    bio: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Author',
    tableName: 'authors',
  });
  return Author;
};