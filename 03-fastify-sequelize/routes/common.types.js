const IdParamType = {
    type: 'integer',
    minimum: 1,
}

const MessageResponseType = {
    type: 'object',
    properties: {
        message: {type: 'string'},
    },
}


module.exports = {
    IdParamType,
    MessageResponseType,
}