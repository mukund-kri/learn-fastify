const { 
    getBooks, 
    getBookById,
    addBook,
    updateBook,
    deleteBook,
} = require('../controllers/book.controller');

const { IdParamType, MessageResponseType } = require('./common.types');

const BookType = {
    type: 'object',
    properties: {
        id: {type: 'integer'},
        isbn: {type: 'string'},
        title: {type: 'string'},
        description: {type: 'string'},
    }
}


const getBooksOptions = {
    method: 'GET',
    url: '/',
    
    schema: {
        response: {
            200: {
                type: 'array',
                items: BookType,
            },
        },
    },
    handler: getBooks,
};

const getBookByIdOptions = {
    method: 'GET',
    url: '/:id',

    schema: {
        description: 'Get a book by id',
        tags: ['books'],
        params: {
            id: IdParamType,
        },
        querystring: {
            title: { type: 'string' },
        },
        response: {
            201: BookType,
            404: MessageResponseType,
        },
    },
    handler: getBookById,
};

const addBookOptions = {
    method: 'POST',
    url: '/',
    schema: {
        description: 'Add a book',
        tags: ['books'],
        body: {
            type: 'object',
            properties: {
                isbn: { type: 'string' },
                title: { type: 'string' },
                description: { type: 'string' },
            },
            required: ['isbn', 'title'],
        },
        response: {
            201: BookType,
            400: MessageResponseType,
        },
    },
    handler: addBook,
};

const deleteBookOptions = {
    method: 'DELETE',
    url: '/:id',
    schema: {
        description: 'Delete a book',
        tags: ['books'],
        params: {
            id: IdParamType,
        },
        response: {
            204: MessageResponseType,
            404: MessageResponseType,
        },
    },
    handler: deleteBook,
};

const updateBookOptions = {
    method: 'PUT',
    url: '/:id',
    schema: {
        description: 'Update a book',
        tags: ['books'],
        params: {
            id: IdParamType,
        },
        body: {
            type: 'object',
            properties: {
                isbn: { type: 'string' },
                title: { type: 'string' },
                description: { type: 'string' },
            },
        },
        response: {
            200: BookType,
            404: MessageResponseType,
        },
    },
    handler: updateBook,
};


const booksRoutes = async (server, options, done) => {

    // get all books
    server.route(getBooksOptions);

    // get a book by id
    server.route(getBookByIdOptions);
    
    // add an new book
    server.route(addBookOptions);

    // update a book
    server.route(updateBookOptions);

    // delete a book
    server.route(deleteBookOptions);

    done();
}

module.exports = booksRoutes;
