const { 
    getAllAuthors,
    getAuthorById,
    addAuthor,
    updateAuthor,
    deleteAuthor
} = require('../controllers/author.controller');
const { IdParamType, MessageResponseType } = require('./common.types');


const AuthorType = {
    type: 'object',
    properties: {
        id: {type: 'integer'},
        firstname: {type: 'string'},
        middlename: {type: 'string'},
        lastname: {type: 'string'},
        honorific: {type: 'string'},
        bio: {type: 'string'},
    },
};

const getAllAuthorsOptions = {
    method: 'GET',
    url: '/',
    schema: {
        response: {
            200: {
                type: 'array',
                items: AuthorType,
            },
            404: MessageResponseType,
        },
    },
    handler: getAllAuthors,
};

const getAuthorByIdOptions = {
    method: 'GET',
    url: '/:id',
    schema: {
        params: {
            id: IdParamType,
        },
        response: {
            200: AuthorType,
            404: MessageResponseType,
        },
    },
    handler: getAuthorById,
};

const addAuthorOptions = {
    method: 'POST',
    url: '/',
    schema: {
        body: {
            type: 'object',
            properties: {
                firstname: {type: 'string'},
                middlename: {type: 'string'},
                lastname: {type: 'string'},
                honorific: {type: 'string'},
                bio: {type: 'string'},
            },
            required: ['firstname', 'lastname', 'honorific', 'bio'],
        },
        response: {
            201: AuthorType,
        },
    },
    handler: addAuthor,
};

const updateAuthorOptions = {
    method: 'PUT',
    url: '/:id',
    schema: {
        params: {
            id: IdParamType,
        },
        body: {
            type: 'object',
            properties: {
                firstname: {type: 'string'},
                middlename: {type: 'string'},
                lastname: {type: 'string'},
                honorific: {type: 'string'},
                bio: {type: 'string'},
            },
        },
        response: {
            200: AuthorType,
            404: MessageResponseType,
        },
    },
    handler: updateAuthor,
};

const deleteAuthorOptions = {
    method: 'DELETE',
    url: '/:id',
    schema: {
        params: {
            id: IdParamType,
        },
        response: {
            200: MessageResponseType,
            404: MessageResponseType,
        }
    },
    handler: deleteAuthor, 
};

const authorRoutes = async (fastify, options, done) => {

    // GET authors
    fastify.route(getAllAuthorsOptions);

    // GET authors by id
    fastify.route(getAuthorByIdOptions);

    // Create an new author
    fastify.route(addAuthorOptions);

    // Update an author
    fastify.route(updateAuthorOptions);

    // Delete an author
    fastify.route(deleteAuthorOptions);

    done();
}

module.exports = authorRoutes;