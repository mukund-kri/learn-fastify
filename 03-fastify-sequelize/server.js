const server = require('fastify')({
    logger: true
});

const booksRoutes = require('./routes/books.routes.js');
const authorRoutes = require('./routes/author.routes.js');

const BASE_API_PATH = "/v1";


// Register the swagger plugin
server.register(require('fastify-swagger'), {
    exposeRoute: true,
    routePrefix: '/docs',
    swagger: {
        info: {
            title: 'Books API',
        },
    }
});

// Register the books routes
server.register(booksRoutes, {prefix: `${BASE_API_PATH}/books`});

// Register the author routes
server.register(authorRoutes, {prefix: `${BASE_API_PATH}/authors`});

// Start the server
server.listen(3000, (err, address) => {
    if (err) {
        server.log.error(err);
        process.exit(1);
    }
    server.log.info(`server listening on ${address}`);
});